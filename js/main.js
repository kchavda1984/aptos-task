$(document).ready(function(e) {
  for (var i=0;i<data.products.length;i++) {
    var asset = data.products[i];
    var nowPrice = asset.price.now.toFixed(2);
    var wasPrice = asset.price.was.toFixed(2)

    if (asset.price.now !== asset.price.was) {
      wasPrice = '<span class="f-product__price f-product__price--was">£' + wasPrice + '</span>';
    } else {
      wasPrice = '';
    }

    $('.row').append('<article class="f-product col-sm-12 col-md-6 col-lg-4"><div class="f-product-holding"><img src="' + asset.image + '" class="f-product__image img-responsive" /><div class="f-product__content"><h2 class="f-product__name">' + asset.name + '</h2><p class="f-product__description">' + asset.description + '</p><p class="f-product__prices">' + wasPrice + '<span class="f-product__price f-product__price--now">&pound;' + nowPrice + '</span></p><p class="f-product__actions clearfix"><a class="f-product__action f-product__action--view btn btn-default" href="#" role="button">View details</a><a class="f-product__action f-product__action--buy btn btn-primary pull-right" href="#" role="button">Buy now</a></p></div></div></article>');
  }
});


