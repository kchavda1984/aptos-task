var data = {
  "products": [
    {
      "name": "Cara suede side zip boot",
      "description": "Updated for the season, our signature Cara Ankle Boots are the perfect addition to your autumn wardrobe.",
      "image": "images/7000434.jpg",
      "price": {
        "was": 149.00,
        "now": 149.00
      }
    },
    {
      "name": "Esme V front show boot",
      "description": "Crafted from butter-soft leather, these stylish boots are made in Italy.",
      "image": "images/7000412.jpg",
      "price": {
        "was": 159.00,
        "now": 159.00
      }
    },
    {
      "name": "Celeste stitch pointed flat",
      "description": "Our Celeste flats are simply beautiful. Crafted from soft suede, they have a pointed toe and subtle stitch detailing.",
      "image": "images/7000409.jpg",
      "price": {
        "was": 89.00,
        "now": 69.00
      }
    }
  ]
};
